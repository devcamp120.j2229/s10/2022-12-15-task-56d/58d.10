package com.devcamp.rainbowrequestinput.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class RainbowController {
    String[] rainbows = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};
    
    @GetMapping("/rainbow-request-query")
    public ArrayList<String> filterColor(@RequestParam(value = "text") String filter) {
        ArrayList<String> result = new ArrayList<>();

        for (String element : rainbows) {
            if(element.toLowerCase().contains(filter.toLowerCase())) {
                result.add(element);
            }
        }

        return result;
    }

    @GetMapping("/rainbow-request-param/{id}")
    public String indexColor(@PathVariable(value = "id") int index) {
        if(index < 0 || index > 6) {
            return null;
        }

        return rainbows[index];
    }

}
