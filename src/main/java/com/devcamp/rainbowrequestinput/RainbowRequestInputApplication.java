package com.devcamp.rainbowrequestinput;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowRequestInputApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowRequestInputApplication.class, args);
	}

}
